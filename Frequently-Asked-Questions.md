#### Where can I see its status, or in what way it impacts network activity?

You can use the [testing utility](https://decentraleyes.org/test) to ensure that Decentraleyes is working as intended. Contextual information on local injections can be found inside of the extension popup. To inspect actual network traffic, use your browser's built-in network monitor, or a [packet analyzer](https://en.wikipedia.org/wiki/Packet_analyzer).

#### What does it do to protect me when it has no choice but to allow a request?

Even if a resource is not locally available, or if the website in question is whitelisted, Decentraleyes offers improved protection by stripping details from intercepted CDN-requests. This keeps specific data, such as what page you are on, from reaching delivery networks.

#### Can CDNs track me even though they do not place tracking cookies?

Definitely. Requests to CDNs contain the “Referer” HTTP header (a misspelling of referrer) that reveals what site and page you're visiting. Techniques like IP address tracking and [browser fingerprinting](https://panopticlick.eff.org/static/browser-uniqueness.pdf) can be used to associate the aggregated data with your identity.

#### My browser caches downloaded CDN libraries, doesn't that protect my privacy?

Unfortunately, no. Even if a resource has been cached, your browser might still contact its distributor to find out if it has been modified.

#### Will it work well in combination with other privacy enhancing add-ons?

Yes. Decentraleyes was designed to integrate seamlessly with all other types of privacy-enhancing extensions including, but definitely not limited to: ClearURLs, Cookie AutoDelete, Disconnect, PopUpOFF, NoScript Security Suite, Privacy Badger and uBlock Origin.