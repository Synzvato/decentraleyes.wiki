## February 2024

**Roboin Blog (Japanese / 日本語)**

- [プライバシーを守る「Decentraleyes」の使い方](https://roboin.io/article/2024/02/26/how-to-use-decentraleyes/)

## December 2023

**XDA**

- [5 great Firefox extensions and tweaks to improve your privacy](https://www.xda-developers.com/top-firefox-extensions-improve-privacy/)

## October 2023

**Stack Diary**

- [15 Browser Extensions to Protect Privacy & Security](https://stackdiary.com/browser-extensions-to-protect-privacy-security/)

## September 2023

**phoenixNAP**

- [18 Best Security Extensions for Chrome](https://phoenixnap.com/blog/best-security-extensions-for-chrome)

## March 2023

**MakeUseOf**

- [The Top 5 Firefox Add-Ons for Streamlined Browsing](https://www.makeuseof.com/firefox-add-ons-streamline-browsing/)

## January 2023

**The New York Times Wirecutter**

- [Our Favorite Ad Blockers and Browser Extensions to Protect Privacy](https://www.nytimes.com/wirecutter/reviews/our-favorite-ad-blockers-and-browser-extensions-to-protect-privacy/)

## June 2022

**TurboLab.it (Italian / Italiano)**

- [Aumenta la tua privacy e velocizza il caricamento delle pagine web con Decentraleyes!](https://turbolab.it/browser-455/aumenta-privacy-velocizza-caricamento-pagine-web-decentraleyes-3603)

## July 2019

**TrishTech.com**

- [Decentraleyes Emulates Local CDN in Firefox and Chrome](https://www.trishtech.com/2019/07/decentraleyes-emulates-local-cdn-in-firefox-and-chrome/)

## March 2019

**MakeUseOf**

- [10 Quick Firefox Tweaks to Maximize Your Online Privacy](https://www.makeuseof.com/tag/firefox-online-privacy-tweaks/)

## February 2019

**YouTube - The Hated One**

- [How to protect your online privacy in 2019](https://www.youtube.com/watch?v=lLessJ4R6w8)

**Rubenerd**

- [Decentraleyes](https://rubenerd.com/decentraleyes/)

## January 2019

**Restore Privacy**

- [Privacy Tools](https://restoreprivacy.com/privacy-tools/)

## August 2018

**Mozilla Blog - The Firefox Frontier**

- [Make your Firefox browser a privacy superpower with these extensions](https://blog.mozilla.org/firefox/make-your-firefox-browser-a-privacy-superpower-with-these-extensions/)

## April 2018

**Restore Privacy**

- [Firefox Privacy – The Complete How-To Guide](https://restoreprivacy.com/firefox-privacy/)

**Kuketz IT-Security Blog (German / Deutsch)**

- [Firefox: Decentraleyes – Firefox-Kompendium Teil3](https://www.kuketz-blog.de/firefox-decentraleyes-firefox-kompendium-teil3/)

## January 2018

**CK’s Technology News**

- [Decentraleyes addon loads CDN resources locally](https://chefkochblog.wordpress.com/2018/01/20/decentraleyes-addon-loads-cdn-resources-locally/)

## November 2017

**That One Guy**

- [Top 5 Privacy Extentions for Google Chrome / Mozilla Firefox](https://thatoneguyy.wordpress.com/2017/11/22/top-5-privacy-extentions-for-google-chrome-mozilla-firefox/)

**CloudBoost**

- [Must-have Security Add-ons for Browsers](https://blog.cloudboost.io/must-have-security-add-ons-for-browsers-ef88b8404635)

**Leo Tindall**

- [Locking Down Firefox](https://leotindall.com/tutorial/locking-down-firefox/)

**BestVPN Privacy News**

- [The Complete Firefox Privacy and Security Guide](https://www.bestvpn.com/firefox-privacy-security-guide/)

## October 2017

**Poorly Reported**

- [6 Essential Firefox Security Add-ons for Normal People](https://www.poorlyreported.com/2017/10/essential-firefox-add-ons/)

**Neowin**

- [Decentraleyes 2.0 supports Firefox 57 and above](https://www.neowin.net/news/decentraleyes-20-supports-firefox-57-and-above)

**Netzgrad (German / Deutsch)**

- [Künstlername - Netzgrad Internet Podcast / Folge 16, Kapitel 3](https://netzgrad.org/ng016-kuenstlername/)

## September 2017

**c't Magazin (German / Deutsch)**

- [Härter surfen - Browser und E-Mail gegen Angriffe absichern](https://www.heise.de/ct/ausgabe/2017-20-Browser-und-E-Mail-gegen-Angriffe-absichern-3827033.html)

**Trang Công Nghệ (Vietnamese / Tiếng Việt)**

- [Hướng dẫn tối ưu bảo mật và riêng tư trên Firefox](https://trangcongnghe.com/thu-thuat/81403-huong-dan-toi-uu-bao-mat-v224-ri234ng-tu-tr234n-firefox.html)

## August 2017

**Make Tech Easier**

- [The Ultimate Firefox Privacy & Security Guide](https://www.maketecheasier.com/ultimate-firefox-privacy-security-guide/)

**gHacks Technology News**

- [Firefox 57: Decentraleyes add-on is compatible now](https://www.ghacks.net/2017/08/29/firefox-57-decentraleyes-add-on-is-compatible-now/)

**Jeroen Baert's Blog (Flemish Dutch / Vlaams-Nederlands)**

- [User Tracking op Vlaamse krantenwebsites (2017)](https://www.forceflow.be/2017/08/02/tracking-be-2017/)

## July 2017

**YouTube - Sebastian Heinze**

- [Essential Add-Ons for Chrome and Firefox](https://www.youtube.com/watch?v=6Jf1EWO5jOE)

## June 2017

**Yale Privacy Lab**

- [Citizen FOSS: Snowden's toolkit — for the Rest of Us.](https://github.com/seandiggity/citizen-foss/raw/master/presentation.pdf)

## May 2017

**Privacyend**

- [The 5 Best Browser Extensions to Protect Your Privacy](https://www.privacyend.com/best-browser-extensions-to-protect-your-privacy/)

**LiquidVPN**

- [How To Protect Your Online Privacy 2017 – Desktop Web Browsers](https://www.liquidvpn.com/online-privacy-desktop-web-browsers/)

## March 2017

**Kreen (Traditional Chinese / 正體中文 繁體)**

- [Decentraleyes - 讓瀏覽器使用電腦端 CDN 資源，加快網頁顯示速度及避免追蹤](https://kreen.org/2311/decentraleyes-loads-cdn-resources-locally)

## December 2016

**BestVPN Privacy News**

- [Decentraleyes Review – a Firefox privacy add-on](https://www.bestvpn.com/privacy-news/decentraleyes-review/)

## September 2016

**Fundamental Software**

- [Tool in the Spotlight: Decentraleyes](https://www.fundamental-software.net/2016/09/tool-in-the-spotlight-decentraleyes/)

## May 2016

**Allan Aguilar (Spanish / Español)**

- [PRISM Break # 2](https://allanaguilar.org/2016/05/16/prismbreak2/)

**W Internecie (Polish / Polski)**

- [Decentraleyes – nowa wtyczka na nowe metody śledzenia](https://winternecie.info/2016/05/decentraleyes-nowa-wtyczka-na-nowe-metody-sledzenia/)

## April 2016

**Apprcn (Simplified Chinese / 中文 简体)**

- [Decentraleyes – 使用本地 CDN 库文件加快网页加载速度[Firefox 扩展]](https://www.apprcn.com/decentraleyes.html)

## March 2016

**Softpedia News**

- [Decentraleyes Firefox Add-On Speeds Up Page Loads by Emulating CDNs Locally](https://news.softpedia.com/news/decentraleyes-firefox-add-on-speeds-up-page-loads-by-emulating-cdns-locally-502361.shtml)

**Bitcoinist**

- [Decentraleyes Addon Fixes Browser Privacy, Circumvents CDNs](https://bitcoinist.com/decentraleyes-addon-fixes-browser-privacy-circumvents-cdns/)

## February 2016

**PrivacyTools.io**

- [Excellent Firefox Privacy Addons](https://www.privacytools.io/#addons)

**PRISM Break (Multilingual)**

- [Free Recommendations - Web Browser Addons](https://prism-break.org/en/all/#web-browser-addons)

**Mozilla Add-ons Blog**

- [February 2016 Featured Add-ons](https://blog.mozilla.org/addons/2016/02/04/february-2016-featured-add-ons)

**Collin M. Barrett**

- [Decentraleyes: Block CDN Tracking](https://collinmbarrett.com/decentraleyes-block-cdn-tracking)

**ADSLZone (Spanish / Español)**

- [Evita que te rastreen al navegar con Mozilla Firefox gracias a estas extensiones](https://www.adslzone.net/2016/02/03/evita-que-te-rastreen-al-navegar-con-mozilla-firefox-gracias-a-estas-extensiones/)

**GIGAZINE (Japanese / 日本語)**

- [CDNの共通ライブラリファイルをローカルに保存してセキュリティ＆閲覧速度を高めるFirefoxアドオン「Decentraleyes」](https://gigazine.net/news/20160202-decentraleyes/)

**The Next Web**

- [Take privacy one step further and use this extension to block content delivery networks](https://thenextweb.com/news/take-privacy-one-step-further-and-use-this-extension-to-block-content-delivery-networks)

## December 2015

**NullPointerException (French / Français)**

- [Extensions Firefox pour protéger sa vie privée](https://blog.imirhil.fr/2015/12/08/extensions-vie-privee.html)

## November 2015

**Korben (French / Français)**

- [Decentraleyes – Bloquer les appels aux CDN sans casser les sites web](https://korben.info/decentraleyes-bloquer-appels-aux-cdn-casser-sites-web.html)

**Psy-Q's Braindump**

- [Decentraleyes: An additional defense against large companies analyzing you](https://psyq123.wordpress.com/2015/11/24/decentraleyes-an-additional-defense-against-large-companies-analyzing-you)

**gHacks Technology News**

- [Decentraleyes for Firefox loads CDN resources locally](https://www.ghacks.net/2015/11/23/decentraleyes-for-firefox-loads-cdn-resources-locally/)
