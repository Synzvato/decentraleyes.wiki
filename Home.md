# General Support
- [Frequently Asked Questions](https://git.synz.io/Synzvato/decentraleyes/-/wikis/Frequently-Asked-Questions)

# Technical Information
- [Simple Introduction](https://git.synz.io/Synzvato/decentraleyes/-/wikis/Simple-Introduction)
- [Supported Networks](https://git.synz.io/Synzvato/decentraleyes/-/wikis/Supported-Networks)
- [Bundled Resources](https://git.synz.io/Synzvato/decentraleyes/-/wikis/Bundled-Resources)

# Miscellaneous
- [Media Coverage](https://git.synz.io/Synzvato/decentraleyes/-/wikis/Media-Coverage)
- [Privacy Policy](https://git.synz.io/Synzvato/decentraleyes/-/wikis/Privacy-Policy)