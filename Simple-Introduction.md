According to statistics, like [these](http://w3techs.com/technologies/overview/content_delivery/all) from W3Techs, a lot of websites make you load vital files through large third-party services like Google Hosted Libraries, and jQuery CDN (StackPath).

There are a couple of reasons why web developers are tempted to do this. It lowers upkeep costs (as these services do not cost the host any money), and it speeds up the web in the sense that if you store a specific version of a file once, you will only contact that central content delivery service to see if the file your browser already has, is identical to the one that's being served.

The fact that these companies are now deeply woven into the fabric of the web, and that cutting them off actually breaks a significant percentage of all websites, caused this add-on to be created. It comes bundled with a fair amount of commonly used files, and serves them locally whenever a site tries to fetch them from a delivery network. This saves bandwidth, and protects your privacy.

Decentraleyes complements regular content blockers (e.g. uBlock Origin, and Adblock Plus).