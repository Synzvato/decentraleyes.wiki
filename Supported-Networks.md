## Operational Networks

| Name                                 | Endpoint                 | Type    | Remark     |
| ------------------------------------ | ------------------------ | ------- | ---------- |
| Baidu&nbsp;CDN                            | `apps.bdimg.com`         | Regular |            |
| Baidu&nbsp;CDN                            | `libs.baidu.com`         | Regular | Deprecated |
| CDNJS&nbsp;_(Cloudflare)_                 | `cdnjs.cloudflare.com`   | Regular |            |
| Geekzu&nbsp;Public&nbsp;Service           | `sdn.geekzu.org`         | Mirror  |            |
| Google&nbsp;Hosted&nbsp;Libraries         | `ajax.googleapis.com`    | Regular |            |
| jQuery&nbsp;CDN _(Fastly)_                | `code.jquery.com`        | Regular |            |
| jsDelivr&nbsp;_(Cloudflare,&nbsp;Fastly)_ | `cdn.jsdelivr.net`       | Regular |            |
| Microsoft&nbsp;Ajax&nbsp;CDN              | `ajax.aspnetcdn.com`     | Regular |            |
| Microsoft&nbsp;Ajax&nbsp;CDN              | `ajax.microsoft.com`     | Regular | Deprecated |
| Sina&nbsp;Public&nbsp;Resources           | `lib.sinaapp.com`        | Regular |            |
| UpYun&nbsp;Library                        | `upcdn.b0.upaiyun.com`   | Regular |            |
| USTC&nbsp;Linux&nbsp;User&nbsp;Group      | `ajax.proxy.ustclug.org` | Mirror  |            |
| Yandex&nbsp;CDN                           | `yandex.st`              | Regular | Deprecated |
| Yandex&nbsp;CDN                           | `yastatic.net`           | Regular |            |

## Dissolved Networks

| Name    | Type    | Remark           |
| ------- | ------- | ---------------- |
| BootCDN | Regular | Compromised [^1] |

[^1]: https://arstechnica.com/security/2024/07/384000-sites-link-to-code-library-caught-performing-supply-chain-attack/